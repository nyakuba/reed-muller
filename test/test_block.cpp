#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Block_test
#include <boost/test/unit_test.hpp>
#include "../src/include/block.h"

BOOST_AUTO_TEST_SUITE(block)

BOOST_AUTO_TEST_CASE(creation) {
  int64_t n = 128;
  Block b(n);
  BOOST_CHECK(b.getN() == n);
  BOOST_CHECK(b.size() == 3);
}

BOOST_AUTO_TEST_CASE(test_eq) {
  int64_t n = 128;
  std::vector<uint64_t> vec1(1, 0), vec2(1, 0), vec3(3, 0), vec4(1, 0);
  vec1.push_back(0xAAAAAAAAAAAAAAAA); vec1.push_back(0xBBBBBBBBBBBBBBBB);
  vec2.push_back(0xAAAAAAAAAAAAAAAA); vec2.push_back(0xBBBBBBBBBBBBBBBB);
  vec4.push_back(0xAAAAAAAAAAAAAAAA); vec4.push_back(0xBBBBBBBBBBBBBBB0);
  Block block1(n, vec1), block2(n, vec2), block3(n, vec3), block4(n, vec4);
  BOOST_CHECK(block1 == block1);
  BOOST_CHECK(block1 == block2);
  BOOST_CHECK(block1 != block3);
  BOOST_CHECK(block1 != block4);
}

BOOST_AUTO_TEST_CASE(test_gt) {
  int64_t n = 128;
  std::vector<uint64_t> vec1(1, 0), vec2(1, 0), vec3(3, 0), vec4(1, 0);
  vec1.push_back(0xAAAAAAAAAAAAAAAA); vec1.push_back(0xBBBBBBBBBBBBBBBB);
  vec2.push_back(0xAAAAAAAAAAAAAAAA); vec2.push_back(0xBBBBBBBBBBBBBBBB);
  vec4.push_back(0xAAAAAAAAAAAAAAAB); vec4.push_back(0xBBBBBBBBBBBBBBBB);
  Block block1(n, vec1), block2(n, vec2), block3(n, vec3), block4(n, vec4);
  BOOST_CHECK(!(block1 < block1));
  BOOST_CHECK(!(block1 < block2));
  BOOST_CHECK(!(block1 < block3));
  BOOST_CHECK(block1 < block4);
}

BOOST_AUTO_TEST_CASE(test_lt) {
  int64_t n = 128;
  std::vector<uint64_t> vec1(1, 0), vec2(1, 0), vec3(3, 0), vec4(1, 0);
  vec1.push_back(0xAAAAAAAAAAAAAAAA); vec1.push_back(0xBBBBBBBBBBBBBBBB);
  vec2.push_back(0xAAAAAAAAAAAAAAAA); vec2.push_back(0xBBBBBBBBBBBBBBBB);
  vec4.push_back(0xAAAAAAAAAAAAAAAA); vec4.push_back(0xBBBBBBBBBBBBBBBC);
  Block block1(n, vec1), block2(n, vec2), block3(n, vec3), block4(n, vec4);
  BOOST_CHECK(!(block1 > block1));
  BOOST_CHECK(!(block1 > block2));
  BOOST_CHECK(block1 > block3);
  BOOST_CHECK(!(block1 > block4));
}

BOOST_AUTO_TEST_CASE(test_add_gf2) {
  int64_t n = 128;
  std::vector<uint64_t> vec1(1, 0), vec2(1, 0), vec3(3, 0);
  vec1.push_back(0xAAAAAAAAAAAAAAAA); vec1.push_back(0xBBBBBBBBBBBBBBBB);
  vec2.push_back(0x1111111111111111); vec2.push_back(0xBBBBBBBBBBBBBBBB);
  Block block1(n, vec1), block2(n, vec2), block3(n, vec3), acc(n, vec3);

  std::vector<uint64_t> rvec1(3, 0), rvec2(1, 0), rvec3(1, 0);
  rvec2.push_back(0xAAAAAAAAAAAAAAAA); rvec2.push_back(0xBBBBBBBBBBBBBBBB);
  rvec3.push_back(0xBBBBBBBBBBBBBBBB); rvec3.push_back(0x0000000000000000);
  Block res1(n, rvec1), res2(n, rvec2), res3(n, rvec3);

  add_gf2(block1, block1, acc);
  BOOST_CHECK(acc == res1);

  add_gf2(block1, block3, acc);
  BOOST_CHECK(acc == res2);

  add_gf2(block1, block2, acc);
  BOOST_CHECK(acc == res3);
}

BOOST_AUTO_TEST_CASE(test_mul_expl) {
  int64_t n = 127;
  std::vector<uint64_t> vec1, vec2;
  vec1.push_back(0x1AAAAAAAAAAAAAAA); vec1.push_back(0xBBBBBBBBBBBBBBBB);
  vec2.push_back(0x1222222222222222); vec2.push_back(0xAAAAAAAAAAAAAAAA);
  Block block1(n, vec1), block2(n, vec2), block3(n), acc(n);

  std::vector<uint64_t> rvec1(2, 0), rvec2, rvec3;
  rvec2.push_back(0x1AAAAAAAAAAAAAAA); rvec2.push_back(0xBBBBBBBBBBBBBBBB);
  rvec3.push_back(0x1222222222222222); rvec3.push_back(0xAAAAAAAAAAAAAAAA);
  Block res1(n, rvec1), res2(n, rvec2), res3(n, rvec3);

  mul_expl(block1, block3, acc);
  BOOST_CHECK(acc == res1);

  mul_expl(block1, block1, acc);
  BOOST_CHECK(acc == res2);

  mul_expl(block1, block2, acc);
  BOOST_CHECK(acc == res3);
}

BOOST_AUTO_TEST_CASE(test_test_bit) {
  int64_t n = 191;
  std::vector<uint64_t> vec;
  vec.push_back(9); vec.push_back(32); vec.push_back(4611686018427387904);
  Block blk(n, vec);
  // block_println(blk);
  BOOST_CHECK(test_bit(blk, 0));
  BOOST_CHECK(!test_bit(blk, 1));
  BOOST_CHECK(test_bit(blk, 69));
  BOOST_CHECK(!test_bit(blk, 70));
  BOOST_CHECK(!test_bit(blk, 68));
  BOOST_CHECK(!test_bit(blk, 191));
  BOOST_CHECK(test_bit(blk, 190));
}

BOOST_AUTO_TEST_CASE(test_test_bit1) {
  int64_t n = 64;
  std::vector<uint64_t> vec;
  vec.push_back(255); vec.push_back(0);
  Block blk(n, vec);
  // block_println(blk);
  BOOST_CHECK(!test_bit(blk, 34));
}

BOOST_AUTO_TEST_CASE(test_get_hi_bit_pos) {
  int64_t n = 127;
  std::vector<uint64_t> vec(1);
  vec.push_back(358830);
  Block blk(n, vec);
  // block_println(blk);
  // int64_t bpos = get_hi_bit_pos(blk);
  // std::cout << bpos << std::endl;
  BOOST_CHECK(get_hi_bit_pos(blk) == 82);

  std::vector<uint64_t> vec1(2, 0);
  Block blk1(n, vec1);
  BOOST_CHECK(get_hi_bit_pos(blk1) == -1);
}

BOOST_AUTO_TEST_CASE(test_exch_bit) {
  {
    int64_t n = 200;
    std::vector<uint64_t> vec(4);
    vec[0] = 1;
    Block block(n, vec);
    std::vector<uint64_t> rvec(4);
    rvec[0] = 16;
    Block res(n, rvec);
    exch(block, 0, 4);
    // block_println(block);
    // block_println(res);
    BOOST_CHECK(block == res);
  }
  {
    int64_t n = 200;
    std::vector<uint64_t> vec(4);
    vec[0] = 2;
    Block block(n, vec);
    std::vector<uint64_t> rvec(4);
    rvec[1] = 32;
    Block res(n, rvec);
    exch(block, 1, 69);
    // block_println(block);
    // block_println(res);
    BOOST_CHECK(block == res);
  }
  {
    int64_t n = 200;
    std::vector<uint64_t> vec(4);
    vec[0] = 32;
    Block block(n, vec);
    std::vector<uint64_t> rvec(4);
    rvec[2] = 8;
    Block res(n, rvec);
    exch(block, 5, 131);
    // block_println(block);
    // block_println(res);
    BOOST_CHECK(block == res);
  }
}

BOOST_AUTO_TEST_CASE(test_set_bits) {
  {
    int64_t n = 40;
    std::vector<uint64_t> vec;
    vec.push_back(1099511627775);
    Block block(n), res(n, vec);
    set_bits(block, 0, n);
    BOOST_CHECK(block == res);
  }
  {
    int64_t n = 40;
    std::vector<uint64_t> vec;
    vec.push_back(1048575);
    Block block(n), res(n, vec);
    set_bits(block, 0, 20);
    BOOST_CHECK(block == res);
  }
  {
    int64_t n = 40;
    std::vector<uint64_t> vec;
    vec.push_back(1099510579200);
    Block block(n), res(n, vec);
    set_bits(block, 20, 40);
    BOOST_CHECK(block == res);
  }
  {
    int64_t n = 40;
    std::vector<uint64_t> vec;
    vec.push_back(1073740800);
    Block block(n), res(n, vec);
    set_bits(block, 10, 30);
    BOOST_CHECK(block == res);
  }
}

BOOST_AUTO_TEST_CASE(test_set_bits1) {
  {
    int64_t n = 255;
    std::vector<uint64_t> vec;
    vec.push_back(18446744073708503040); vec.push_back(-1); vec.push_back(-1); vec.push_back(255); 
    Block block(n), res(n, vec);
    set_bits(block, 20, 200);
    // block_println(block);
    // block_println(res);
    BOOST_CHECK(block == res);
  }
}

BOOST_AUTO_TEST_CASE(test_shift_left) {
  {
    int64_t n = 256;
    std::vector<uint64_t> vec(5, 0);
    vec[0] = 9223372036854775808;
    vec[1] = 9223372036854775808;
    Block blk(n, vec);
    std::vector<uint64_t> rvec(5, 0);
    rvec[1] = 1;
    rvec[2] = 1;
    Block res(n, rvec);

    // bvector_println(blk);
    shift_left(blk);
    // bvector_println(blk);
    // bvector_println(res);
    BOOST_CHECK(blk == res);
  }
}

BOOST_AUTO_TEST_SUITE_END()
