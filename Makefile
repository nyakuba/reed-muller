CC = icpc
COPT = -W -Wall -std=c++11
COPTIMIZATION = -axSSE4.2 \
                    -fast \
		    -xHost \
		    -use-intel-optimized-headers \
		    -parallel
INCL = -I./src/include
MAIN = src/main.cpp
CPP = \
src/block.cpp \
src/coder.cpp \
src/decoder.cpp \
src/line.cpp \
src/rmcode.cpp
LIBS = -lm

TEST_CPP = \
test/test_block \
test/test_rmcode
TEST_LIBS = -lboost_unit_test_framework
TEST_OPTS = --log_level=test_suite

OUT = rmcode

MKDIR = mkdir -p

all:
	make release

debug: $(MAIN) $(CPP)
	$(MKDIR) bin
	$(MKDIR) log
	$(CC) -g $(COPT) $(MAIN) $(CPP) -o bin/debug_$(OUT) $(LIBS) 2> log/debug.log
#	bin/debug_$(OUT)

release: $(MAIN) $(CPP)
	$(MKDIR) bin
	$(MKDIR) log
	$(CC) $(COPT) $(COPTIMIZATION) $(MAIN) $(CPP) -o bin/$(OUT) $(LIBS) -DNDEBUG 2> log/release.log
#	bin/$(OUT)

tests: compile-tests run-tests
run-tests:
	for test in $(TEST_CPP); do \
		bin/$$test $(TEST_OPTS); \
	done

compile-tests: $(TEST_DEPS)
	$(MKDIR) bin
	$(MKDIR) bin/test
	for test in $(TEST_CPP); do \
		$(CC) -g $(COPT) $$test.cpp $(CPP) -o bin/$$test $(LIBS) $(TEST_LIBS); \
	done

clean:
	rm -rf bin/ log/

cleanlog: 
	rm -rf log/
