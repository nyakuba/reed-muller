#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <chrono>
#include <random>

#include "include/common.h"
#include "include/coder.h"
#include "include/decoder.h"
#include "include/line.h"
#include "include/rmcode.h"

struct config {
public:
  config() : N(1), R(1), test_count(1), mode(0), err_count(0), err_prob(0), logging(true), err_info(true) {}
  int64_t N;
  int64_t R;
  int64_t test_count;
  int mode;
  int64_t err_count;
  int64_t err_prob;
  std::string input_filename;
  bool logging;
  bool err_info;
  std::string output_filename;
  void println() {
    std::cout << "N " << N << ", R " << R 
	      << ", testc " << test_count 
	      << ", mode " << mode << ", err-count " << err_count << ", err-prob " << err_prob
	      << ", in " << input_filename << ", no-err-info " << err_info
	      << ", no-log " << !logging << ", out " << output_filename << std::endl;
  }
};

int read_config(int argc, char ** args, config & c);
bool cmp(Block & dec_word, Block & word);

int main(int argc, char *argv[]) {

  config c;
  read_config(argc, argv, c);

  std::ifstream in(c.input_filename);
  if (!in.is_open()) {
    std::cerr << "error opening file" << std::endl;
    return -1;
  }
  in >> c.N >> c.R;
  c.println();
  std::cout << "press enter to start" << std::endl;
  std::cin.get();

  ReedMullerCode rmcode(c.N, c.R);
  rmcode.form_matrix();
  std::cout << "RM code (" << c.N << ", " << rmcode.getNrows() << ")" << std::endl;
  if (c.logging) {
    matrix_println(rmcode, 0);
    matrix_println(rmcode, 1);
    matrix_println(rmcode, 2);
    matrix_println(rmcode, 4);
    std::cout << std::endl;
  }

  std::vector<uint64_t> vec;
  int64_t nums = 0;
  uint64_t tmp;
  in >> nums;
  for (int i = 0; i < nums; ++i) {
    in >> tmp;
    vec.push_back(tmp);
  }
  Block word(rmcode.getNrows(), vec);
  Block err(c.N);
  std::vector<uint64_t> err_vec;
  if (c.mode == 0) {
    in >> nums;
    for (int i = 0; i < nums; ++i) {
      in >> tmp;
      err_vec.push_back(tmp); // 1000
    }
    err = Block(c.N, err_vec);
  }

  int success = 0;
  int no_decoded = 0;
  int fail = 0;
  std::chrono::duration<double> total_time;
  for (int i = 0; i < c.test_count; ++i) {
    Block enc_word(c.N);
    Block dec_word(rmcode.getNrows());

    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed_seconds;

    start = std::chrono::system_clock::now();
    rmcode.encode(word, enc_word);  
    end = std::chrono::system_clock::now();

    elapsed_seconds = end-start;
    total_time += elapsed_seconds;
    if (c.logging) {
      block_println(word);
      block_println(enc_word);
      block_println(err);
    }
    
    int err_count = 0;
    if (c.mode == 1) {
      err_count = c.err_count;
      unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
      std::mt19937 g(seed);
      // std::cout << "mt " << g1() << std::endl;
      // std::cout << "mt " << g1() << std::endl;
      Block tmp(c.N);
      for (int i = 0; i < c.err_count; ++i) {
	uint64_t rand = g()%c.N;
	while (test_bit(tmp, rand)) {
	  ++rand; rand %= c.N;
	}
	set_bits(tmp, rand%c.N, rand%c.N + 1);
      }
      err = tmp;
    } else if (c.mode == 2) {
      unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
      std::mt19937 g (seed);
      Block tmp(c.N);
      for (int i = 0; i < c.N; ++i) {
	if ((g()%100) <= c.err_prob) {
	  ++err_count;
	  set_bits(tmp, i, i + 1);
	}
      }
      err = tmp;
    }

    // std::cout << "err" << std::endl;
    // block_println(err);

    add_gf2(enc_word, err, enc_word);

    start = std::chrono::system_clock::now();
    bool flag = rmcode.decode(enc_word, dec_word);
    end = std::chrono::system_clock::now();

    elapsed_seconds = end-start;
    total_time += elapsed_seconds;
    if (!flag) {
      ++no_decoded;
      if (c.logging || c.err_info) {
	std::cout << "cannot decode: err_count : " << err_count << std::endl;
	bvector_println(err);
      }
    } else if (dec_word != word) {
      if (c.logging || c.err_info) {
	std::cout << "fail! err_count : " << err_count << std::endl;
	// bvector_println(word);
	// bvector_println(dec_word);
      }
      ++fail;
    } else {
      if (c.logging) {
	std::cout << "successfull." << std::endl;
      }
      ++success;
    }
    if (c.logging) {
      std::cout << "dec" << std::endl;
      block_println(dec_word);
      std::cout << "word" << std::endl;
      block_println(word);
    }
  }
  std::cout << "Not decoded : " << no_decoded << "/" << c.test_count << std::endl;
  std::cout << "Fail : " << fail << "/" << c.test_count << std::endl;
  std::cout << "Success : " << success << "/" << c.test_count << std::endl;
  std::cout << "Enc/dec time : " << total_time.count() << "s" << std::endl;

  return 0;
}

int read_config(int argc, char ** argv, config & c) {
  int i = 1;
  while (i < argc) {
    if (strcmp(argv[i], "--in") == 0) {
      c.input_filename = argv[i + 1]; i+=2;
    } else if (strcmp(argv[i], "--out") == 0) {
      c.output_filename = argv[i + 1]; i+=2;
    } else if (strcmp(argv[i], "--mode") == 0) {
      c.mode = std::atoi(argv[i + 1]); i+=2;
    } else if (strcmp(argv[i], "--err-count") == 0) {
      c.err_count = std::atoi(argv[i + 1]); i+=2;
    } else if (strcmp(argv[i], "--err-prob") == 0) {
      c.err_prob = std::atoi(argv[i + 1]); i+=2;
    // } else if (strcmp(argv[i], "-n") == 0) {
    //   c.N = std::atoi(argv[i + 1]); i+=2;
    // } else if (strcmp(argv[i], "-r") == 0) {
    //   c.R = std::atoi(argv[i + 1]); i+=2;
    } else if (strcmp(argv[i], "--testc") == 0) {
      c.test_count = std::atoi(argv[i + 1]); i+=2;
    } else if (strcmp(argv[i], "--no-logs") == 0) {
      c.logging = false; ++i;
    } else if (strcmp(argv[i], "--no-err-info") == 0) {
      c.err_info = false; ++i;
    } else {
      std::cerr << "bad config" << std::endl;
      return -1;
    }
  }
  return 0;
}
