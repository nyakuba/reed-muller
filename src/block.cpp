#include "include/block.h"
#include <iostream>
#include <bitset>

Block::Block(int64_t n): _vector(n/(sizeof(uint64_t)*8) + 1), _n(n) { }
Block::Block(int64_t n, const std::vector<uint64_t> & vec): _vector(n/(sizeof(uint64_t)*8) + 1), _n(n) {
  assert(n <= vec.size()*sizeof(uint64_t)*8);
  assert(vec.size() == _vector.size());
  auto vecit = vec.begin();
  auto it = _vector.begin();
  auto end = vec.end();
  while (vecit < end) {
    *it = *vecit;
    ++it, ++vecit;
  }
}
Block::Block(const Block & blk): _n(blk._n) {
  this->_vector = blk._vector;
}

Block & Block::operator=(const Block & blk) {
  assert(this->_vector.size() == blk._vector.size());
  this->_vector = blk._vector;
  return *this;
}

void Block::set_zero() {
  for (auto && el : _vector)
    el = 0;
}

bool operator==(const Block & b1, const Block & b2) {
  assert(b1._vector.size() == b2._vector.size());
  if (&b1 == &b2) return true;
  auto dit = b1._vector.begin();
  auto wit = b2._vector.begin();
  auto end = b1._vector.end();
  while (dit < end) {
    if (*dit != *wit)
      return false;
    ++dit, ++wit;
  }
  return true;
}

bool operator<(const Block & b1, const Block & b2) {
  assert(b1._vector.size() == b2._vector.size());
  if (&b1 == &b2) return false;
  auto dit = b1._vector.end()-1;
  auto wit = b2._vector.end()-1;
  auto end = b1._vector.begin();
  while (dit >= end) {
    if (*dit < *wit)
      return true;
    --dit, --wit;
  }
  return false;  
}

bool operator>(const Block & b1, const Block & b2) {
  assert(b1._vector.size() == b2._vector.size());
  if (&b1 == &b2) return false;
  auto dit = b1._vector.end()-1;
  auto wit = b2._vector.end()-1;
  auto end = b1._vector.begin();
  while (dit >= end) {
    if (*dit > *wit)
      return true;
    --dit, --wit;
  }
  return false;  
}

bool operator!=(const Block & b1, const Block & b2) {
  return !(b1 == b2);
}

void copy(const Block & from, Block & to) {
  if (&from != &to) {
    auto to_it = to._vector.begin();
    auto to_end = to._vector.end();
    auto from_it = from._vector.begin();
    auto from_end = from._vector.end();
    while (to_it < to_end && from_it < from_end) {
      *to_it = *from_it;
      ++to_it; ++from_it;
    }
  }
}

void add_gf2(const Block & lhs, const Block & rhs, Block & res) {
  if (!(lhs._vector.size() == rhs._vector.size() && lhs._vector.size() == res._vector.size())) 
    std::cerr << "add_gf2: different sizes" << std::endl;
  auto lhs_it = lhs._vector.begin(); 
  auto rhs_it = rhs._vector.begin();
  auto res_it = res._vector.begin();
  auto end = lhs._vector.end();
  if (&lhs == &res) {
    while (lhs_it < end) {
      *res_it ^= *rhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }    
  } else if (&rhs == &res) {
    while (lhs_it < end) {
      *res_it ^= *lhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }
  } else {
    while (lhs_it < end) {
      *res_it = *lhs_it ^ *rhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }
  }
}

void mul_expl(const Block & lhs, const Block & rhs, Block & res) {
  if (!(lhs._vector.size() == rhs._vector.size() && lhs._vector.size() == res._vector.size())) 
    std::cerr << "add_gf2: different sizes" << std::endl;
  auto lhs_it = lhs._vector.begin(); 
  auto rhs_it = rhs._vector.begin();
  auto res_it = res._vector.begin();
  auto end = lhs._vector.end();
  if (&lhs == &res) {
    while (lhs_it < end) {
      *res_it &= *rhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }    
  } else if (&rhs == &res) {
    while (lhs_it < end) {
      *res_it &= *lhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }
  } else {
    while (lhs_it < end) {
      *res_it = *lhs_it & *rhs_it;
      ++lhs_it, ++rhs_it, ++res_it;
    }
  }
}

int64_t popcnt(const Block & blk) {
  auto it = blk._vector.begin();
  auto end = blk._vector.end();
  int64_t weight = 0;
  while (it < end) {
    uint64_t tmp = *it;
    while (tmp != 0) {
      tmp &= tmp - 1;
      ++weight;
    }
    ++it;
  }
  // std::cout << weight << std::endl;
  return weight;
}

void exch(Block & block, int64_t index1, int64_t index2) {
  assert(index1 <= block._n && index1 >= 0);
  assert(index2 <= block._n && index2 >= 0);
  if (index1 == index2) return;
  int64_t bitsize = sizeof(uint64_t)*8;  
  int64_t i1 = index1/bitsize, i2 = index2/bitsize;
  int64_t i1_bit = index1 - i1*bitsize;
  int64_t i2_bit = index2 - i2*bitsize;
  uint64_t mask1 = 1 << i1_bit;
  uint64_t mask2 = 1 << i2_bit;
  uint64_t res1 = block._vector[i1]&mask1;
  uint64_t res2 = block._vector[i2]&mask2;
  if (res1 == 0 && res2 != 0) {
    block._vector[i1] |= mask1;
    block._vector[i2] ^= mask2;
  } else if (res1 != 0 && res2 == 0) {
    block._vector[i1] ^= mask1;
    block._vector[i2] |= mask2;
  }
}

void set_bits(Block & block, int64_t lo, int64_t hi) {
  assert(lo < hi);
  assert(hi <= block._n);
  assert(lo >= 0);
  int64_t bitsize = sizeof(uint64_t)*8;
  int64_t start_index = lo/bitsize, start_bit = lo - start_index*bitsize;
  int64_t end_index = hi/bitsize, end_bit = hi - end_index*bitsize;
  // std::cout << start_index << " " << start_bit << std::endl;
  // std::cout << end_index << " " << end_bit << std::endl;
  if (start_index == end_index) {
    int64_t diff = end_bit - start_bit;
    uint64_t mask = 0;
    for (int i = 0; i < diff; ++i) {
      mask <<= 1;
      mask += 1;
    }
    mask <<= start_bit;
    // std::cout << mask << std::endl;
    block._vector[start_index] |= mask;
  } else {
    int64_t diff = bitsize - start_bit;
    uint64_t mask = 0;
    for (int i = 0; i < diff; ++i) {
      mask <<= 1;
      mask += 1;
    }
    mask <<= start_bit;
    block._vector[start_index] |= mask;
    ++start_index;
    mask = -1;
    while (start_index < end_index) {
      block._vector[start_index] = mask;
      ++start_index;
    }
    mask = 0;
    for (int i = 0; i < end_bit; ++i) {
      mask <<= 1;
      mask += 1;
    }
    // ++start_index;
    block._vector[start_index] |= mask;
  }
}

bool test_bit(const Block & blk, int64_t pos) {
  int64_t bitsize = sizeof(uint64_t)*8;
  int64_t index = pos/bitsize;
  int64_t bit = pos - index*bitsize;
  uint64_t mask = 1;
  mask <<= bit;
  return (blk._vector[index] & mask) != 0;
}

int64_t get_hi_bit_pos(const Block & blk) {
  int64_t bitsize = sizeof(uint64_t)*8;
  auto && beg = blk._vector.rend();
  for (auto && it = blk._vector.rbegin(); it < beg; ++it) {
    if (*it != 0) {
      int64_t pos = bitsize - 1;
      uint64_t mask = 1;
      mask <<= pos;
      // std::bitset<64> bs(mask);
      // std::cout << "m " << bs << " it " << *it << std::endl;
      // std::cout << pos << std::endl;
      while ((mask & *it) == 0) {
	mask >>= 1; --pos;	
      }
      // std::cout << pos << std::endl;
      return (beg - it - 1)*bitsize + pos;
    }
  }
  return -1;
}

void block_println(const Block & block) {
  // std::cout << block._vector.size() << " : ";
  std::bitset<sizeof(uint64_t)*8> binary_repr;
  auto it = block._vector.rbegin();
  auto beg = block._vector.rend();
  int first_bits = sizeof(uint64_t)*8*block.size() - block._n;
  binary_repr = *it;

  for (int i = sizeof(uint64_t)*8 - first_bits - 1; i >= 0; --i)
    std::cout << binary_repr[i] << " ";
  ++it;
  while (it < beg) {
    binary_repr = *it;
    for (int i = sizeof(uint64_t)*8 - 1; i >= 0; --i)
      std::cout << binary_repr[i] << " "; 
    ++it;
  }
  std::cout << std::endl;
}

void block_println(const Block & block, const std::vector<int64_t> & col_order) {
  std::cout << block._vector.size() << " : ";
  // block_println(block);
  for (auto && num : col_order) {
    // std::cout << ":" << num << " " << test_bit(block, num) << std::endl;
    if (test_bit(block, num)) {
      std::cout << 1 << "";
    } else {
      std::cout << 0 << "";
    }
  }
  std::cout << std::endl;
}

void bvector_println(const Block & block) {
  std::bitset<sizeof(uint64_t)*8> binary_repr;
  auto it = block._vector.rbegin();
  auto beg = block._vector.rend();
  while (it < beg) {
    binary_repr = *it;
    std::cout << binary_repr << " "; 
    ++it;
  }
  std::cout << std::endl;
}

void copy_block(Block & blk, int64_t src_lo, int64_t src_hi, int64_t dest_lo) {
  assert(src_lo < src_hi);
  int64_t diff = src_hi - src_lo;
  assert((src_hi <= dest_lo) || (dest_lo + diff < src_lo));
  for (int i = 0; i < diff; ++i) {
    if (test_bit(blk, src_lo + i)) {
      set_bits(blk, dest_lo + i, dest_lo + i + 1);
    }
  }
}

void shift_left(Block & blk) {
  uint64_t hi_bit = 1;
  hi_bit <<= (sizeof(uint64_t)*8 - 1);
  auto it = blk._vector.begin();
  auto end = blk._vector.end();
  bool carry = false;
  bool c = false;
  while (it < end) {
    c = carry;
    if ((*it & hi_bit) != 0) carry = true;
    else carry = false;
    *it <<= 1;
    if (c)
      ++(*it);
    ++it;
  }
}
