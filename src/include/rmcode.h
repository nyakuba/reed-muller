#ifndef _RMCODE_H_
#define _RMCODE_H_

#include "common.h"

/**
   Represents Reed-Muller (n, r) code.
 */
class ReedMullerCode {
 public:
  ReedMullerCode(int64_t n, int64_t r);

  int64_t getN() const {return _n;}
  int64_t getK() const {return _k;}
  int64_t getR() const {return _r;}
  int64_t getNrows() const {return _G.size();}

  void form_matrix();
  void encode(const Block & blk, Block & enc);
  bool decode(const Block & blk, Block & dec);

  friend void matrix_println(const ReedMullerCode & code, int64_t sys_view = 2);
  static bool next_comb(int64_t k, std::vector<int64_t> & comb);

 private:
  const int64_t _n;
  int64_t _k;
  const int64_t _r;

  std::vector<  Block >  _G;
  std::vector<  Block >  _Gsys;
  std::vector<  Block >  _Gcorr;
  std::vector<  Block >  _Gsums;
  std::vector< int64_t > _col_order;

  int64_t dim();
  void formG();
  void col_exch();
  void back_gauss(std::vector<std::pair<int, int> > & corr);
  void formSumMatrix();
  void add_sum_mask(const Block *pblk, int block_len);
  bool correct(const Block & blk, Block & dec);
  static void sort(std::vector<Block> & g, 
		   std::vector<std::pair<int, int> > & corr, 
		   const std::vector<Block>::iterator & glo, 
		   const std::vector<Block>::iterator & ghi);
  static void sort(std::vector<Block> & g, 
		   std::vector<std::pair<int, int> > & corr, 
		   int64_t lo, 
		   int64_t hi);
  static void swap(Block & a, Block & b);
  static void transpose(std::vector<Block> & g);
  static void mul_matrix(std::vector<Block> & m1, const std::vector<Block> & m2);
};

#endif /* _RMCODE_H_ */
