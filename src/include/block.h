#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <vector>
#include "common.h"

/**
   Block - is a vector of uint64_t, that represents sequence of elements in GF(2) (bits).
*/
class Block {
 public:
  /// Constructor
  Block(int64_t n);
  Block(int64_t n, const std::vector<uint64_t> & vec);
  Block(const Block & blk);

  /// Getters
  uint64_t size() const { return _vector.size(); }
  uint64_t getN() const { return _n; }

  /// sets block value to 0
  void set_zero();

  /// Operators
  Block & operator=(const Block & blk);
  friend bool operator==(const Block & b1, const Block & b2);
  friend bool operator!=(const Block & b1, const Block & b2);
  friend bool operator<(const Block & b1, const Block & b2);
  friend bool operator>(const Block & b1, const Block & b2);

  /// copy from block with different size
  friend void copy(const Block & from, Block & to);

  /// wedge sum in gf2.
  friend void add_gf2(const Block & lhs, const Block & rhs, Block & res);

  /// wedge product in gf2.
  friend void mul_expl(const Block & lhs, const Block & rhs, Block & res);

  /// element exch. index points directly to <b>bit</b> position in sequence.
  /// zero-based numeration.
  friend void exch(Block & block, int64_t index1, int64_t index2);

  /// prints entire block.
  friend void block_println(const Block & block);
  friend void block_println(const Block & block, const std::vector<int64_t> & col_order);
  friend void bvector_println(const Block & block);

  /// sets bits to 1 on given range.
  /// zero-based numeration. lo - first bit to set, hi - first not modified bit.
  friend void set_bits(Block & block, int64_t lo, int64_t hi);

  /// copyes all setted bits from one block area to another
  friend void copy_block(Block & blk, int64_t src_lo, int64_t src_hi, int64_t dest_lo);

  /// test if bit on position = pos is one
  friend bool test_bit(const Block & blk, int64_t pos);

  /// returns position of highest bit in block
  friend int64_t get_hi_bit_pos(const Block & blk);

  /// hamming weight
  friend int64_t popcnt(const Block & blk);

  /// shift left on one position
  friend void shift_left(Block & blk);

 private:
  std::vector<uint64_t> _vector;
  const uint64_t _n;
};

#endif /* _BLOCK_H_ */
