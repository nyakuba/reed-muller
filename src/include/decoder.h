#ifndef _DECODER_H_
#define _DECODER_H_

#include "common.h"
#include "rmcode.h"

void decode(const ReedMullerCode & code, const Block & enc_word, Block & dec_word);

#endif /* _DECODER_H_ */
