#ifndef _LINE_H_
#define _LINE_H_

#include "common.h"
#include "block.h"

/**
   This file contain function, that used to represent line
*/

/// error addition to given sequence of bits.
void add_errors(Block & word);

#endif /* _LINE_H_ */
