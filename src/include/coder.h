#ifndef _CODER_H_
#define _CODER_H_

#include "common.h"
#include "rmcode.h"

void code(const ReedMullerCode & code, const Block & word, Block & enc_word);

#endif /* _CODER_H_ */
