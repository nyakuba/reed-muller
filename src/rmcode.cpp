#include <algorithm>
#include <iostream>

#include "include/rmcode.h"

ReedMullerCode::ReedMullerCode(int64_t n, int64_t r): _n(n), _k(0), _r(r) { 
  int64_t tmp = n;
  while (tmp > 0) {
    tmp /= 2; ++_k;
  }
  --_k;
}

int64_t ReedMullerCode::dim() {
  if (_r == 0) return 1;
  int64_t acc = _k + 1, comb = _k;
  for (int i = 1; i < _r; ++i) {
    comb *= _k - i; comb /= i + 1;
    acc += comb;
    // std::cout << acc << std::endl;
  }
  return acc;
}

void matrix_println(const ReedMullerCode & code, int64_t mode /* = 2 */) {
  std::cout << "n " << code._n << ", r " << code._r  << std::endl;
  if (mode == 0) {
    std::cout << "system" << std::endl;
    for (auto && row : code._Gsys) {
      block_println(row);
    }
  } else if (mode == 1) {
    std::cout << "initial" << std::endl;
    for (auto && row : code._G) {
      block_println(row);
    }
  } else if (mode == 2) {
    std::cout << "correction" << std::endl;
    for (auto && row : code._Gcorr) {
      block_println(row);
    }
  } else if (mode == 3) {
    std::cout << "system + col_order" << std::endl;
    for (auto && row : code._Gsys) {
      block_println(row, code._col_order);
    }
  } else if (mode == 4) {
    std::cout << "sums" << std::endl;
    for (auto && row : code._Gsums) {
      block_println(row);
    }
  }
}

void ReedMullerCode::form_matrix() {
  // Формирование матрицы кода.
  _G.resize(dim(), Block(_n));
  formG();

  // Копирование матрицы.
  _Gsys = _G;
    
  // Сортируем строки как длинные числа по убыванию.
  // И одновременно сохраняем историю преобразований.
  std::vector<std::pair<int, int> > corr;
  sort(_Gsys, corr, _Gsys.begin(), _Gsys.end());

  // Заполняем матрицу элементарных преобразований.
  _Gcorr.resize(_G.size(), Block(_G.size()));
  {
    auto corr_it = _Gcorr.begin();
    auto size = _Gcorr.size();
    int64_t i = 0;
    while (i < size) {
      set_bits(*corr_it, size - i - 1, size - i); 
      ++i; ++corr_it;
    }
  }
  {
    auto corr_end = corr.rend();
    for (auto i = corr.rbegin(); i < corr_end; ++i) {
      swap(_Gcorr[(*i).first], _Gcorr[(*i).second]);
    }
  }

  // Создание вспомогательного вектора индексов столбцов.
  _col_order.resize(_n);
  // Сначала переставить столбцы, чтобы получить главную диагональ,
  // Перестановка - это просто нужный порядок столбцов.
  col_exch();

  // Очистка истории.
  corr.clear();
  // После этого обратный ход Гаусса.    
  back_gauss(corr);

  ////////////////////////////////
  std::vector<Block> _Gcorr1(_G.size(), Block(_G.size()));
  {
    auto corr_it = _Gcorr1.begin();
    auto size = _Gcorr1.size();
    int64_t i = 0;
    while (i < size) {
      set_bits(*corr_it, size - i - 1, size - i); 
      ++i; ++corr_it;
    }
  }
  {
    auto corr_end = corr.rend();
    for (auto i = corr.rbegin(); i < corr_end; ++i) {
      add_gf2(_Gcorr1[(*i).first], _Gcorr1[(*i).second], _Gcorr1[(*i).second]);
    }
  }
  // for (auto && blk : _Gcorr1) 
  //   block_println(blk);
  ////////////////////////////////

  mul_matrix(_Gcorr, _Gcorr1);
  transpose(_Gcorr);

  // Транспонируем матрицу в систематическом виде, столбцы матрицы содержались в строках.
  // Это увеличит эффективность кодирования.
  transpose(_Gsys);

  // Для исправления ошибок нужны проверочные суммы.
  // Создадим матрицу строки которой будут содержать позиции битов для контрольных сумм.
  formSumMatrix();
}

void ReedMullerCode::formSumMatrix() {
  Block first(_n); set_bits(first, 0, 1);
  _Gsums.push_back(first);
  if (_r == 0) return;
  int64_t acc = 1; int64_t comb = _k;
  for (int i = 1; i <= _r; ++i) {
    int sum_count = 1; // количество проверочных сумм
    for (int j = 0; j < _k - i; ++j)
      sum_count *= 2;
    int block_len = _n/sum_count; // количество бит в проверочной сумме
    // std::cout << "comb scount blen " << comb << " " << sum_count << " " << block_len << std::endl;
    for (int j = 0; j < comb; ++j) { // цикл по строкам исходной матрицы
      Block *pblk = &_G[acc + j];
      add_sum_mask(pblk, block_len); // добавление суммы в матрицу
    }
    acc += comb;
    comb *= _k - i; comb /= i + 1;
  }
}

void ReedMullerCode::add_sum_mask(const Block *pblk, int block_len) {
  // std::cout << "pblk" << std::endl;
  // block_println(*pblk);
  Block mask(_n);
  set_bits(mask, 0, 1);
  int w = 1, bits_set = 1;
  while (w < _n && bits_set < block_len) {
    if (!test_bit(*pblk, w)) {
      copy_block(mask, 0, w, w);
      bits_set *= 2;
    }
    w *= 2;
  }
  // block_println(mask);
  _Gsums.push_back(mask);
}

void ReedMullerCode::formG() {
  auto && it = _G.begin();
  set_bits(*it, 0, _n);
  ++it;
  if (_r >= 1) {
    int64_t bsize = _n/2;
    for (int i = 0; i < _k; ++i) { // TODO: can remove _k. _n bounded by zero.
      for (int j = 0; j < _n; j += bsize*2) {
	set_bits(*it, j, j+bsize);
      }
      ++it; bsize /= 2;
    }
  }
  if (_r > 1) {
    auto && it = _G.begin() + _k + 1;
    for (int i = 2; i <= _r; ++i) {
      std::vector<int64_t> comb(i);
      int j = 0;
      for (auto && el : comb)
	el = j++;
      bool flag = true;
      while (flag) {
	*it = _G[comb[0]+1];
	for (int l = 0; l < i; ++l)
	  mul_expl(*it, _G[comb[l]+1], *it);
	flag = next_comb(_k-1, comb);
	++it;
      }
    }
  }
}

void ReedMullerCode::col_exch() {
  int l = _n - 1;
  auto c_it = _col_order.begin();
  auto cend = _col_order.end();
  while (c_it < cend) {
    *c_it = l--;
    ++c_it;
  }
  for (uint64_t i = 0; i < _Gsys.size(); ++i) { 
    if (!test_bit(_Gsys[i], _n - i - 1)) {
      int64_t hi = get_hi_bit_pos(_Gsys[i]);
      std::swap(_col_order[i], _col_order[_n - hi - 1]);
    }
  }
}

void ReedMullerCode::back_gauss(std::vector<std::pair<int, int> > & corr) {
  auto end = _Gsys.rend();
  auto beg = _Gsys.rbegin();
  int64_t size = _Gsys.size();
  auto it = _Gsys.rbegin();
  for (int64_t i = _n - size; i < _n; ++i) {
    auto iter = it + 1;
    while (iter < end) {
      if (test_bit(*iter, _col_order[_n - i - 1])) {
	add_gf2(*it, *iter, *iter);
	corr.push_back(std::pair<int, int>(size - (it - beg) - 1, size - (iter - beg) - 1));
      }
      ++iter; // ++iter_corr;
    }
    ++it; // ++it_corr;
  }
}

void ReedMullerCode::transpose(std::vector<Block> & g) {
  std::vector<Block> tmp(g);

  // for (auto && b : tmp)
  //   block_println(b);

  int64_t size = g.size();
  int64_t n = g[0].getN();
  g.clear();
  g.resize(n, Block(size));
  for (int64_t i = 0; i < size; ++i) {
    Block *pblk = &(tmp[i]);
    for (int64_t j = 0; j < n; ++j) {
      if (test_bit(*pblk, n - j - 1)) {
	// std::cout << _n - j - 1 << " " << size - i - 1 << std::endl;
	set_bits(g[j], size - (i+1), size - i);
      }
    }
  }
}

bool ReedMullerCode::next_comb(int64_t k, std::vector<int64_t> & comb) {
  auto end = comb.end();
  for (auto && it = comb.begin(); it < end; ++it) {
    if (it == end - 1 && *it == k) return false;
    else if ((it == end - 1) || ((*(it+1) - *it) > 1)) {
      ++(*it);
      int64_t start = 0;
      for (auto && it1 = comb.begin(); it1 < it; ++it1) {
	*it1 = start++;
      }
      return true;
    }
  }
  return false;
}

// enc should be = 0
void ReedMullerCode::encode(const Block & blk, Block & enc) {
  enc.set_zero();
  assert(blk.getN() == _G.size());
  assert(enc.getN() == _n);
  Block tmp(blk.getN());
  for (int64_t i = 0; i < _n; ++i) {
    mul_expl(blk, _Gsys[i], tmp);
    if (popcnt(tmp) % 2 != 0) {
      set_bits(enc, _n - i - 1, _n - i);
    }
  }
}

bool ReedMullerCode::decode(const Block & blk, Block & dec) {
  dec.set_zero();
  // Исправляем ошибки.
  if (!correct(blk, dec)) return false;

  // std::cout << "dec" << std::endl;
  // block_println(dec);
  // Получаем исходное информационное слово умножением полученных бит на матрицу коррекции.
  int64_t size = _Gcorr.size();
  Block tmp(size);
  Block tmp_dec(size);
  for (int i = 0; i < size; ++i) {
    int w = 0;
    for (int j = 0; j < size; ++j) {
      if (test_bit(dec, j) && test_bit(_Gcorr[i], size - j - 1))
    	w ^= 1;
    }
    // mul_expl(dec, _Gcorr[i], tmp);
    // if (popcnt(tmp) % 2 != 0)
    //   set_bits(tmp_dec, size - i - 1, size - i);
    if (w == 1) {
      set_bits(tmp_dec, size - i - 1, size - i);
    }
  }
  dec = tmp_dec;
  return true;
}

bool ReedMullerCode::correct(const Block & blk, Block & dec) {
  // std::cout << "error correction" << std::endl;
  if (_r == 0) {
    int weight = popcnt(blk);
    if (weight > _n/2) set_bits(dec, 0, 1);
    return true;
  }
  bool flag = true;
  int64_t acc = 1, comb = _k;
  for (int i = 1; i <= _r; ++i) {
    acc += comb;
    comb *= _k - i; comb /= i + 1;
  }
  comb *= _r + 1; comb /= _k - _r; 

  // std::cout << acc << " " << comb << std::endl;

  Block tmp(blk);
  for (int i = _r; i >= 1; --i) {
    int sum_count = 1; // количество проверочных сумм
    for (int j = 0; j < _k - i; ++j)
      sum_count *= 2;
    int block_len = _n/sum_count; // количество битов в блоке
    // std::cout << "comb " << comb << std::endl;

    for (int j = 0; j < comb; ++j) {
      // std::cout << "index" << acc-j-1 << std::endl;

      Block *pblk = &_G[acc - j - 1];
      Block mask = _Gsums[acc - j - 1];

      // std::cout << "pblk" << std::endl;
      // block_println(*pblk);

      Block res(_n);
      int low_bit = 0, maj_weight = 0;
      for (int k = 0; k < sum_count; ++k){
	while (!test_bit(*pblk, low_bit)) {
	  shift_left(mask); ++low_bit;
	}

	// bvector_println(mask);

	mul_expl(mask, tmp, res);
	if (popcnt(res) % 2 != 0)
	  ++maj_weight;
	shift_left(mask); ++low_bit;
      } // for sums

      // std::cout << "maj_weight " << maj_weight << std::endl;

      // std::cout << acc-j-1 << " " << sum_count << std::endl;
      int64_t mid = sum_count/2;
      if (maj_weight == mid)
	flag = false;
      if (maj_weight > mid)
	set_bits(dec, acc-j-1, acc-j);
    } // for rows
    // Получение искаженного кодового слова.
    // Умножаем dec на блок.

    Block corr(_n);
    for (int col = 0; col < _n; ++col) {
      int bit = 0;
      for (int row = 0; row < comb; ++row) {
	// block_println(_G[acc - comb + row]);
	// std::cout << acc - row - 1 << std::endl;
	// if (test_bit(dec, acc - row - 1) && test_bit(_G[acc - row - 1], col))
	if (test_bit(dec, acc - comb + row) && test_bit(_G[acc - comb + row], col))
	  bit ^= 1;
      }
      // std::cout << std::endl;
      if (bit == 1) set_bits(corr, col, col + 1);
    }
    add_gf2(corr, tmp, tmp);
    // std::cout << comb << std::endl;
    // block_println(dec);
      
    // std::cout << "tmp - corr" << std::endl;
    // block_println(tmp);

    acc -= comb;
    comb *= i; comb /= _k - i + 1; 
  } // for blocks
  int weight = popcnt(tmp);
  if (weight == _n/2) 
    flag = false;
  if (weight > _n/2) 
    set_bits(dec, 0, 1);
  return flag;
}

void ReedMullerCode::sort(std::vector<Block> & g, 
			  std::vector<std::pair<int, int> > & corr, 
			  const std::vector<Block>::iterator & glo, 
			  const std::vector<Block>::iterator & ghi) {
  if (ghi - glo < 2) return;
  auto beg = g.begin();
  auto i = glo;
  auto j = glo;
  auto k = ghi;
  while (j < k) {
    if (*j == *i) { ++j; }
    else if (*j > *i) { swap(*j, *i); ++i; ++j; corr.push_back(std::pair<int, int>(i - beg - 1, j - beg - 1)); }
    else if (*j < *i) { --k; swap(*j, *k); corr.push_back(std::pair<int, int>(j - beg, k - beg)); }
  }
  sort(g, corr, glo, i);
  sort(g, corr, j, ghi);
}

void ReedMullerCode::sort(std::vector<Block> & g, 
			  std::vector<std::pair<int, int> > & corr, 
			  int64_t lo, 
			  int64_t hi) {
  if (hi - lo < 2) return;
  auto i = lo;
  auto j = lo;
  auto k = hi;
  while (j < k) {
    if (g[j] == g[i]) { ++j; }
    else if (g[j] > g[i]) { swap(g[j], g[i]); ++i; ++j; corr.push_back(std::pair<int, int>(i-1, j-1)); }
    else if (g[j] < g[i]) { --k; swap(g[j], g[k]); corr.push_back(std::pair<int, int>(j, k)); }
  }
  sort(g, corr, lo, i);
  sort(g, corr, j, hi);
}

void ReedMullerCode::swap(Block & a, Block & b) {
  Block tmp(a);
  a = b;
  b = tmp;
}

// result will be in m1.
void ReedMullerCode::mul_matrix(std::vector<Block> & m1, const std::vector<Block> & m2) {
  std::vector<Block> tmp(m2);
  std::vector<Block> tmp1(m1);
  transpose(tmp);
  Block elem(m2.size());
  for (int64_t i = 0; i < m1.size(); ++i) {
    Block tmp_elem(m2.size());
    for (int64_t j = 0; j < m2[0].getN(); ++j) {
      mul_expl(tmp1[i], tmp[j], elem);
      if (popcnt(elem) % 2 != 0) {
	set_bits(tmp_elem, m2[0].getN() - (j + 1), m2[0].getN() - j);
      }
    }
    m1[i] = tmp_elem;
  }
}
